# gqlproto

Small prototype showing what it might look like to use gqlgen instead of graphql-go

To build:

```bash
go install github.com/99designs/gqlgen/...
go generate .
go build
```

Export `HTTP_PORT` to choose the port manually, or else `gqlproto` defaults to `:7070`.
