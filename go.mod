module gitlab.com/slashclee/gqlproto

go 1.14

require (
	github.com/99designs/gqlgen v0.11.3
	github.com/gorilla/websocket v1.4.2
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/mitchellh/mapstructure v1.3.1 // indirect
	github.com/rs/cors v1.6.0
	github.com/urfave/cli v1.20.0 // indirect
	github.com/vektah/gqlparser v1.3.1
	github.com/vektah/gqlparser/v2 v2.0.1
)
