package main

//go:generate gqlgen generate

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"gitlab.com/slashclee/gqlproto/graph"
	"gitlab.com/slashclee/gqlproto/models"
	"gitlab.com/slashclee/gqlproto/resolver"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
)

func port() string {
	port := os.Getenv("HTTP_PORT")
	if port != "" {
		return fmt.Sprintf(":%s", port)
	}
	return ":7070"
}

func main() {
	/*
		c := cors.New(cors.Options{
			AllowedOrigins:   []string{"http://localhost:7070"},
			AllowCredentials: false,
		})
	*/

	r := resolver.New()
	h := handler.NewDefaultServer(graph.NewExecutableSchema(r))
	go func() {
		t := time.NewTicker(10 * time.Second)
		for i := 0; ; i++ {
			select {
			case <-t.C:
				log.Printf("generating event #%d\n", i)
				e := &models.SeriesAddedEvent{Series: &models.Series{Name: fmt.Sprintf("Series %v", t)}}
				resolver.InjectSeriesEvent(e)
			}
		}
	}()
	http.Handle("/", playground.Handler("Todo", "/query"))
	http.Handle("/query", h)
	log.Fatal(http.ListenAndServe(port(), nil))
}
