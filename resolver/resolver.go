package resolver

import (
	"context"
	"fmt"
	"sync"

	"gitlab.com/slashclee/gqlproto/graph"
	"gitlab.com/slashclee/gqlproto/models"
)

// Resolver is the base struct for graphql responses
type Resolver struct {
	subscribers []chan *models.SeriesAddedEvent
	mutex       sync.Mutex
	seriesList  []*models.Series
}

// Query returns the query resolver
func (r *Resolver) Query() graph.QueryResolver {
	return &queryResolver{r}
}

// Subscription returns the query resolver
func (r *Resolver) Subscription() graph.SubscriptionResolver {
	return &subscriptionResolver{r}
}

type queryResolver struct{ *Resolver }

// Series returns the list of series
func (r *queryResolver) Series(ctx context.Context, uuid *string, offset *int, limit *int) ([]*models.Series, error) {
	return r.seriesList, nil
}

type subscriptionResolver struct{ *Resolver }

func (r *subscriptionResolver) SeriesAdded(ctx context.Context) (<-chan *models.SeriesAddedEvent, error) {
	events := make(chan *models.SeriesAddedEvent, 1)
	go func() {
		<-ctx.Done()
		fmt.Printf("client gone, removing from subscribers\n")
		r.mutex.Lock()
		var i int
		l := len(r.subscribers)
		for i = range r.subscribers {
			if r.subscribers[i] == events {
				break
			}
		}
		r.subscribers[i] = r.subscribers[l-1]
		r.subscribers = r.subscribers[:l-1]
		r.mutex.Unlock()
	}()

	r.mutex.Lock()
	r.subscribers = append(r.subscribers, events)
	r.mutex.Unlock()

	return events, nil
}

var privateResolver *Resolver

func InjectSeriesEvent(s *models.SeriesAddedEvent) {
	if privateResolver == nil {
		return
	}
	for _, sub := range privateResolver.subscribers {
		sub <- s
	}
}

// New returns a newly-created Config object
func New() graph.Config {
	privateResolver = &Resolver{
		subscribers: make([]chan *models.SeriesAddedEvent, 0),
		seriesList:  make([]*models.Series, 0),
	}
	return graph.Config{
		Resolvers: privateResolver,
	}
}
